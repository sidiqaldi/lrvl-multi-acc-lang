<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="{{ $description or '' }}">
  <meta name="author" content="{{ $author or '' }}">
  <meta name="keyword" content="{{ $keyword or '' }}">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <link rel="shortcut icon" href="{{ asset('favicon.png') }}">
  <title>@lang('general.register') - {{ config('app.name', 'Laravel') }}</title>

  <link href="{{ asset('coreui/flag-icon-css/css/flag-icon.min.css') }}" rel="stylesheet">
  <link href="{{ asset('coreui/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
  <link href="{{ asset('coreui/simple-line-icons/css/simple-line-icons.css') }}" rel="stylesheet">
  <link href="{{ asset('coreui/src/css/style.css') }}" rel="stylesheet">

</head>

<body class="app flex-row align-items-center">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-6">
        <div class="card mx-4">
          <div class="card-body p-4">
            <form class="form-horizontal" method="POST" action="{{ route('register') }}">
            {{ csrf_field() }}
            <h1>@lang('general.register')</h1>
            <p class="text-muted">@lang('general.create_your_account')</p>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="icon-user"></i></span>
                </div>
                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus placeholder="@lang('general.name')">
                @if ($errors->has('name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>

            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">@</span>
                </div>
                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required placeholder="@lang('general.email')">
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>

            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="icon-lock"></i></span>
                </div>
                <input id="password" type="password" class="form-control" name="password" required placeholder="@lang('general.password')">
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>

            <div class="input-group mb-4">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="icon-lock"></i></span>
                </div>
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required placeholder="@lang('general.confirm_password')">
            </div>

            <button type="button" class="btn btn-block btn-success">@lang('general.register')</button>
          </form>
          </div>
          <div class="card-footer p-4">
            <div class="row">
              <div class="col-6">
                <a class="btn btn-block btn-google-plus" href="{{ route('login.google') }}">
                  <span>Google</span>
                </a>
              </div>
              <div class="col-6">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap and necessary plugins -->
  <script src="{{ asset('coreui/jquery/dist/jquery.min.js') }}"></script>
  <script src="{{ asset('coreui/popper.js/dist/umd/popper.min.js') }}"></script>
  <script src="{{ asset('coreui/bootstrap/dist/js/bootstrap.min.js') }}"></script>

</body>
</html>
