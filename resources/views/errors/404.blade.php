<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="{{ $description or '' }}">
  <meta name="author" content="{{ $author or '' }}">
  <meta name="keyword" content="{{ $keyword or '' }}">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <link rel="shortcut icon" href="{{ asset('favicon.png') }}">
  <title>{{ config('app.name', 'Laravel') }}</title>

  <link href="{{ asset('coreui/flag-icon-css/css/flag-icon.min.css') }}" rel="stylesheet">
  <link href="{{ asset('coreui/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
  <link href="{{ asset('coreui/simple-line-icons/css/simple-line-icons.css') }}" rel="stylesheet">
  <link href="{{ asset('coreui/src/css/style.css') }}" rel="stylesheet">

</head>

<body class="app flex-row align-items-center">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-6">
        <div class="clearfix">
          <h1 class="float-left display-3 mr-4">404</h1>
          <h4 class="pt-3">Oops! You're lost.</h4>
          <p class="text-muted">The page you are looking for was not found.</p>
        </div>
        <div class="input-prepend input-group">
          <div class="input-group-prepend">
            <span class="input-group-text"><i class="fa fa-search"></i></span>
          </div>
          <input id="prependedInput" class="form-control" size="16" type="text" placeholder="What are you looking for?">
          <span class="input-group-append">
            <button class="btn btn-info" type="button">Search</button>
          </span>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap and necessary plugins -->
  <script src="{{ asset('coreui/jquery/dist/jquery.min.js') }}"></script>
  <script src="{{ asset('coreui/popper.js/dist/umd/popper.min.js') }}"></script>
  <script src="{{ asset('coreui/bootstrap/dist/js/bootstrap.min.js') }}"></script>

</body>
</html>