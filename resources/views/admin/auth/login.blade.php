<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="{{ $description or '' }}">
  <meta name="author" content="{{ $author or '' }}">
  <meta name="keyword" content="{{ $keyword or '' }}">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <link rel="shortcut icon" href="{{ asset('favicon.png') }}">
  <title>{{ config('app.name', 'Laravel') }}</title>

  <link href="{{ asset('coreui/flag-icon-css/css/flag-icon.min.css') }}" rel="stylesheet">
  <link href="{{ asset('coreui/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
  <link href="{{ asset('coreui/simple-line-icons/css/simple-line-icons.css') }}" rel="stylesheet">
  <link href="{{ asset('coreui/src/css/style.css') }}" rel="stylesheet">

</head>

<body class="app flex-row align-items-center">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-8">
        <div class="card-group">
          <div class="card p-4">
            <div class="card-body">
                <form class="form-horizontal" method="POST" action="{{ route('admin.login.submit') }}">
                    {{ csrf_field() }}
                    <h1>Admin Login</h1>
                    <p class="text-muted">@lang('general.sign_in_account')</p>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                        <span class="input-group-text"><i class="icon-user"></i></span>
                        </div>
                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
                        @if ($errors->has('email'))
                          <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                          </span>
                        @endif
                    </div>
                    <div class="input-group mb-4">
                        <div class="input-group-prepend">
                        <span class="input-group-text"><i class="icon-lock"></i></span>
                        </div>
                        <input id="password" type="password" class="form-control" name="password" required>
                        @if ($errors->has('password'))
                          <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                          </span>
                        @endif
                    </div>
                    <div class="input-group mb-4">
                        <label>
                          <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> @lang('general.remember_me')
                        </label>
                    </div>
                    <div class="row">
                        <div class="col-6">
                          <button type="submit" class="btn btn-primary px-4">@lang('general.login')</button>
                        </div>
                        <div class="col-6 text-right">
                          <a href="{{ route('admin.password.request') }}" class="btn btn-link px-0">@lang('general.forgot_password')</a>
                        </div>
                    </div>
                </form>
            </div>
          </div>
          <div class="card text-white bg-primary py-5 d-md-down-none" style="width:44%">
            <div class="card-body text-center">
              <div>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap and necessary plugins -->
  <script src="{{ asset('coreui/jquery/dist/jquery.min.js') }}"></script>
  <script src="{{ asset('coreui/popper.js/dist/umd/popper.min.js') }}"></script>
  <script src="{{ asset('coreui/bootstrap/dist/js/bootstrap.min.js') }}"></script>

</body>
</html>