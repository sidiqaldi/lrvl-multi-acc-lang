<div class="sidebar">
    <nav class="sidebar-nav">
        <ul class="nav">
            <li class="nav-item">
                <a class="nav-link " href="{{ url('admin') }}">
                    <i class="icon-speedometer"></i> @lang('admin.dashboard')
                </a>
            </li>
            <li class="nav-title">@lang('admin.front_page')</li>
            <li class="nav-item nav-dropdown">
                <a class="nav-link nav-dropdown-toggle " href="{{ url('admin/pages') }}">
                    <i class="icon-puzzle"></i> @lang('admin.pages')
                </a>
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a class="nav-link " href="{{ url('admin/pages/create') }}">
                            <i class="icon-puzzle"></i> @lang('admin.pages_add')
                        </a>
                    </li>
                </ul>
            </li>            
        </ul>
    </nav>
    <button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div>