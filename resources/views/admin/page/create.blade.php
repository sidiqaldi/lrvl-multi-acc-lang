@extends('admin.admin')

@section('breadcrumb')
    @component('admin.component.breadcrumb')
    @slot('crumb')
        <li class="breadcrumb-item">Admin</li>
        <li class="breadcrumb-item"><a href="{{ route('pages.index') }}">@lang('admin.pages')</a></li>
        <li class="breadcrumb-item"><a href="{{ route('pages.create') }}">@lang('admin.pages_add')</a></li>
    @endslot
        <a class="btn" href="#"><i class="icon-settings"></i> &nbsp;Settings</a>
    @endcomponent
@endsection

@section('content')
<div class="container">
    
</div>
@endsection
