@extends('admin.admin')

@section('breadcrumb')
    @component('admin.component.breadcrumb')
    @slot('crumb')
        <li class="breadcrumb-item">Admin</li>
        <li class="breadcrumb-item"><a href="{{ url('/') }}">@lang('admin.pages')</a></li>
    @endslot
        <a class="btn" href="#"><i class="icon-speech"></i></a>
        <a class="btn" href="./"><i class="icon-graph"></i> &nbsp;Dashboard</a>
        <a class="btn" href="#"><i class="icon-settings"></i> &nbsp;Settings</a>
    @endcomponent
@endsection

@section('content')
<div class="container">
    
</div>
@endsection
