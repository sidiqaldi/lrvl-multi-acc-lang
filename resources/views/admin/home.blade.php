@extends('admin.admin')

@section('breadcrumb')
    @component('admin.component.breadcrumb')
    @slot('crumb')
        <li class="breadcrumb-item">Admin</li>
        <li class="breadcrumb-item"><a href="{{ url('/') }}">@lang('admin.dashboard')</a></li>
    @endslot
        <a class="btn" href="#"><i class="icon-speech"></i></a>
        <a class="btn" href="./"><i class="icon-graph"></i> &nbsp;Dashboard</a>
        <a class="btn" href="#"><i class="icon-settings"></i> &nbsp;Settings</a>
    @endcomponent
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Admin Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
