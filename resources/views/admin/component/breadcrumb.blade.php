<ol class="breadcrumb">
    {{ $crumb }}
    <li class="breadcrumb-menu d-md-down-none">
        <div class="btn-group" role="group" aria-label="Button group">
            {{ $slot }}
        </div>
    </li>
</ol>