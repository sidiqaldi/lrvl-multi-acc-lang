<?php

return [

    'name' => 'Nama',
    'email' => 'E-mail',
    'email_address' => 'Alamat E-mail',
    'password' => 'Sandi',
    'confirm_password' => 'Konfirmasi sandi',
    'language' => 'Bahasa',
    'login' => 'Masuk',
    'logout' => 'Keluar',
    'register' => 'Daftar',
    'remember_me' => 'Ingatkan Saya',
    'forgot_password' => 'Lupa sandi anda?',
    'sign_in_account' => 'Masuk ke dalam akun anda',
    'create_your_account' => 'Buat akun anda',
    'create_account' => 'Buat akun',
    'back_to_home' => 'Kembali ke halaman utama',

];
