<?php
return [
    'failed'   => 'Email/sandi tersebut tidak cocok dengan data kami.',
    'throttle' => 'Terlalu banyak usaha masuk. Silahkan coba lagi dalam :seconds detik.',
];
