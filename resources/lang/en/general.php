<?php

return [

    'name' => 'Name',
    'email' => 'E-mail',
    'email_address' => 'E-mail address',
    'password' => 'Password',
    'confirm_password' => 'Confirm password',
    'language' => 'Language',
    'login' => 'Login',
    'logout' => 'Logout',
    'register' => 'Register',
    'remember_me' => 'Remember me',
    'forgot_password' => 'Forgot your password?',
    'sign_in_account' => 'Sign In to your account',
    'create_your_account' => 'Create your account',
    'create_account' => 'Create account',
    'back_to_home' => 'Back to home',
];
