<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Core\Page;

class PageController extends Controller
{
    public function index(Request $request, $slug='')
    {
        if ($slug=='')
        {
            return view('welcome');
        }

        $page = Page::where('slug',$slug)->first();
        if ($page) {
            return $page;
        } else {
            abort(404);
        }
    }   
}
