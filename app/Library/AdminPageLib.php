<?php

namespace App\Library;

use App\Models\Core\Config;
use App\Traits\Localization;
use App;

class AdminPageLib 
{
    use Localization;
    
    public static function getAdminHeaderMenu()
    {
        $headerMenu = self::adminHeaderMenu();
        if (isset($headerMenu)){
            return ($headerMenu); 
        }
    }

    public static function getAdminSidebarMenu()
    {
        $sidebarMenu = self::adminSidebarMenu();
        if (isset($sidebarMenu)){
            return ($sidebarMenu); 
        }
    }


    public static function getPageOption($page, $item)
    {
        $options = self::pageOptions();
        if (isset($options[$page][$item])){
            return ($options[$page][$item]); 
        }
    }

    public static function pageOptions()
    {
        return [
            'dashboard' => [
                'breadcrumb' => [
                    'Admin' => [
                        'custom' => '#!'
                    ],
                    __('admin.dashboard') => [
                        'custom' => '#!'
                    ]
                ],
            ],
            'page' => [
                'breadcrumb' => [
                    'Admin' => [
                        'url' => 'admin'
                    ],
                    __('admin.pages') => [
                        'custom' => '#!'
                    ]
                ],
            ],
        ];
    }

    public static function adminHeaderMenu()
    {
        return [
            __('admin.front_page') => [
                'url' => '/'
            ],
        ];
    }
   
    public static function adminSidebarMenu()
    {
        return [
            __('admin.dashboard') => [
                'url' => 'admin',
                'active' => 'admin/dashboard',
                'icon' => 'icon-speedometer'
            ],
            __('admin.front_page') => [
                'type' => 'section',
                'content' => [
                    __('admin.pages') => [
                        'type' => 'sub-menu',
                        'url' => route('pages.index'),
                        'active' => 'admin/pages/*',
                        'icon' =>  'icon-puzzle',
                        'content' => [
                            __('admin.pages_add') => [
                                'url' => route('pages.create'),
                                'active' => 'admin/pages/create',
                                'icon' =>  'icon-puzzle',
                            ]
                        ],
                    ],
                ]
            ],
        ];
    }
}
