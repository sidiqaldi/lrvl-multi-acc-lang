<?php

namespace App\Library;

use App\Models\Core\Config;
use App\Traits\Localization;
use App;
use function GuzzleHttp\json_decode;

class ConfigLib 
{
    use Localization;

    public static function getOption($name)
    {
        $config = Config::where('name',$name)->first();
        if ($config) {
            return json_decode($config['value'],true);
        }
    }

    public static function getMenu()
    {
        $menu = self::getOption('menu');
        $lang = new ConfigLib;
      
        return $lang->getBilingual($menu); 
    }

}
