<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Model;

class Config extends Model
{
    protected $table = 'configs';
}
