<?php

namespace App\Traits;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use App;

trait Localization
{
    public function setLang(Request $request)
    {
        $lang = (!empty($request->lang)) ? $request->lang : Cookie::get('lang');

        switch (\strtolower($lang)) {
            case 'id':
                $locale = 'id';
                break;
            
            default:
                $locale = 'en';
                break;
        }

        $cookie =$request->cookie('lang');

        Cookie::queue('lang', $locale, 36000);
        App::setLocale($locale);

        // dd($cookie);
    }

    public function isBilingual($content = '')
    {
        if (is_array($content) AND isset($content['en'])){
            return true;
        }else{
            return false;
        } 
    }

    public function getBilingual($content = '')
    {
        if ( $this->isBilingual($content) ) 
        {
            $active_lang = App::getLocale();
            if (isset($content[$active_lang])) {
                return $content[$active_lang];
            }
            return $content['en'];
        }

        return $content;
    }
    
}
